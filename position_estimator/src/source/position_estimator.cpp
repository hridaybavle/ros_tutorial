//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"

#include "droneMsgsROS/vector2Stamped.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/PoseStamped.h"
#include "nav_msgs/Path.h"


using namespace std;


//All the required variables
bool velocity_data_available = false;
double current_time_vel =0;
double prev_time_vel;
double velocity_x =0, velocity_y =0, deltaT_vel=0;

bool imu_data_available = false;
double current_time_imu =0;
double prev_time_imu;
double imu_ax =0, imu_ay =0, imu_az =0, deltaT_imu =0;

double x =0, y = 0, z =0;
std::vector<geometry_msgs::PoseStamped> pose_vec;

//initialize the pubs and subs as global variables
ros::Publisher drone_pose_pub;
ros::Publisher drone_path_pub;

ros::Subscriber velocity_data_sub;
ros::Subscriber imu_data_sub;

//initialize all the required functions here
void velocityCallback(const droneMsgsROS::vector2Stamped& msg);
void imuCallback(const sensor_msgs::Imu& msg);

void calculatePose();

int main(int argc,char **argv)
{
  //Init the ROS node here
  ros::init(argc, argv, "position_estimator");
  ros::NodeHandle nh;

  //Put your two publishers here

  //drone_pose_pub = ;
  //drone_path_pub = ;


  //put your two subsribers here

  //velocity_data_sub = ;
  //imu_data_sub      = ;

  //ROS Spin
  while(ros::ok())
  {
    ros::spinOnce();

  }

  return 1;
}

//Include the camera callback here
void velocityCallback(const droneMsgsROS::vector2Stamped &msg)
{

  prev_time_vel = current_time_vel;
  current_time_vel = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

  deltaT_vel = current_time_vel - prev_time_vel;

  //read the topic as msg.vector.x and similarly for y and fill the velocity_x and velocity_y variables
  //two lines
  //velocity_x = ;
  //velocity_y = ;

  //call the function calculate pose
  //one line

}

//imu callback here
void imuCallback(const sensor_msgs::Imu &msg)
{

  prev_time_imu = current_time_imu;
  current_time_imu = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

  deltaT_imu = current_time_imu - prev_time_imu;

  //read the imu topic as msg.linear_acceleration.x for x, y and z and fill the imu_ax, imu_ay and imu_az variables
  // three lines


  imu_data_available = true;

}


void calculatePose()
{
  //x = x + vx * dt + 0.5*ax*dt²
  //y = y + vy * dt + 0.5*ay*dt²
  //y = y + vy * dt + 0.5*ay*dt²

  if (velocity_data_available && imu_data_available)
  {
    //calculate x and y using the formula provided
    //2 lines
    //x = ;
    //y = ;

    cout << "x position is: " << x << endl;
    cout << "y position is: " << y << endl;

    velocity_data_available = false;
    imu_data_available = false;

    ros::Time current_time  = ros::Time::now();

    geometry_msgs::PoseStamped drone_pose;
    drone_pose.header.frame_id = "map";
    drone_pose.header.stamp = current_time;


    //insert calculate x and y position in drone_pose.pose.poistion.x and same for y
    //two lines


    //publish drone_pose_pub
    //one line


    nav_msgs::Path drone_path;
    drone_path.header.frame_id = "map";
    drone_path.header.stamp = current_time;
    pose_vec.push_back(drone_pose);

    //insert pose_vec in drone_path as drone_path.poses
    //one line

    //publish drone_path_pub
    //one line

  }

}








