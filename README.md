# ----- PROGRAMMING WITH BEBOP DRONE -------

In this tutorial we will use the Bebop drone velocities and accelerations to estimate the pose of the drone. The idea of this package to integrate the velocities and the acclerations from the bebop drone in order to estimate its position in X and Y axis using the given formula:

- X = X + Vx * deltaT_v + 0.5 * Ax * (deltaT_a)^0.5
- Y = Y + Vy * deltaT_v + 0.5 * Ay * (deltaT_a)^0.5

# Follow the instructions in order to setup this package:

# 1. create a workspace
- mkdir -p ~/workspace/ros/ros_tutorial/src && cd ~/workspace/ros/ros_tutorial/src

# 2. clone the repo and its dependencies
- git clone https://bitbucket.org/hridaybavle/ros_tutorial.git
- ./ros_tutorial/tello_Alumnos/setupScript.sh

# 3. Build the package
- cd ../../ && catkin_make 

# 4. Start the rosmaster
- roscore

# 4. Play the rosbag in a new terminal
- cd ~/workspace/ros/ros_tutorial/src/ros_tutorial/position_estimator/rosbag && rosbag play bebop_square_trajectory.bag --loop

# 5. Check the topics of the rosbag in a new terminal
- rostopic list
- rqt_image_view for checking the images recorded from the bebop camera

# 6. Check the rostopic type of the topics to be used for calculating the position 
- rostopic type /drone4/ground_speed
- rostopic type /imu

# Open the cpp file
- cd ~/workspace/ros/ros_tutorial/src/ros_tutorial/position_estimator && gedit src/source/position_estimator.cpp 

# 7. Create subsribers for both the topics
- follow this link for creating a subsriber - http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
- create subsriber for both the topics with their proper type

# 8. Calculate the positions and Publish
- calculate the positions of the drone integrating the velocities and accelerations from the formula mentioned before
- publish the position as a ros topic with name "drone_pose" of type geometry_msgs/PoseStamped.msg (http://docs.ros.org/lunar/api/geometry_msgs/html/msg/PoseStamped.html)

# 9. Compute the path 
- save the computed position at each time stamp in a vector and publish this vector as ros path with topic name "drone_path" and type nav_msgs/Path.msg (http://docs.ros.org/melodic/api/nav_msgs/html/msg/Path.html)

# 10. Compile and run the code
- cd ~/workspace/ros/ros_tutorial && source devel/setup.bash
- catkin_make
- rosrun position_estimator_ros position_estimator_ros_node

# 11. Visualize in RVIZ
- once you have done the computation and published the topics you can open rviz and visualize the pose and the path of the drone
- cd ~/workspace/ros/ros_tutorial/src/ros_tutorial/position_estimator/rviz && rviz -d rviz_config.rviz

# ----- PROGRAMMING WITH TELLO DRONE -------

# 1. Open Python file 
- cd ~/workspace/ros/ros_tutorial/src/ros_tutorial/tello_Alumnos/ && gedit joystick_incompleted.py

# 2. Add Publishers for publishing the commands in Python
- follow this link - http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

# 3. Connect the Tello 
- Turn on the tello and connect with it by wifi

# 4. Launch all the files 
- cd ~/workspace/ros/ros_tutorial && source devel/setup.bash
- roslaunch tello_interface tello_interface.launch
- cd ~/workspace/ros/ros_tutorial && source devel/setup.bash
- cd ~/workspace/ros/ros_tutorial/src/ros_tutorial/tello_Alumnos/ && python joystick_incompleted.py

# 5. Control Commands
- t - takeoff
- l - land
- arrow keys - for moving the tello
 



